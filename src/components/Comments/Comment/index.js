import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Avatar, Container } from "@material-ui/core";

const useStyles = makeStyles(() => ({
  root: {
    color: 'white',
    display: 'flex',
    width: '95%',
    margin: 5,
    boxShadow: '0px 0px 1px 0px white',
    position: "relative"
  },
  avatar: {
    maxWidth: "80px",
    minWidth: "80px",
    display: 'flex',
    flexDirection: 'column',
    alignItems: "center",
  },
  p: {
    margin: 5,
    padding: 0,
  },
  commentBody: {
    display: 'flex',
    justifyContent: 'flex-start',
    flexDirection: 'column',
    margin: 15
  },
  text: {
    margin: 0,
    padding: 0
  },
  name: {
    fontSize: "8px"
  },
  data: {
    fontSize: '10px',
    position: "absolute",
    bottom: 0,
    right: 4
  }
}));

export const Comment = ({ text, name, data }) => {
  const classes = useStyles();
  return (
    <Container className={classes.root}>
      <div className={classes.avatar}>
        <Avatar className={classes.p}><p className={classes.name}>{name}</p></Avatar>
      </div>
      <div className={classes.commentBody}>
        <h5 className={classes.text}>{text}</h5>
      </div>
      <div className={classes.data}>
        <h6>
          {new Date(data).getDay()[0] < 10 ? new Date(data).getDay() : <span>0{new Date(data).getDay()}</span>}.
          {new Date(data).getMonth()[0] < 10 ? new Date(data).getMonth() : <span>0{new Date(data).getMonth()}</span>}.
          {new Date(data).getFullYear()}
        </h6>
      </div>
    </Container>
  );
};
