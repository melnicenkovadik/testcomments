import React, { useEffect, useState } from "react";
import { useFetchFromAPI } from "../../hooks/useFetch";
import CircularIndeterminate from "../Loading/Loading";
import { makeStyles } from "@material-ui/core/styles";
import { Comments } from "../Comments";
import { Comment } from "../Comments/Comment";
import { Button } from "@material-ui/core";
import * as url from "url";

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: "center",
    '& > * + *': {
      marginLeft: theme.spacing(2),
    },
  },
}));
export const App = () => {
  const classes = useStyles();
  const [data,
    setData,
    showMore,
    setShowMore,
    page,
    prevPage,
    setState,
    nextPage,
    lastPage,
    state,
    loading,
    setUrl] = useFetchFromAPI();
  const show = () => {
    setUrl(nextPage);
    setShowMore(true);
    if (showMore) {
      setData(prev => {
        return [...prev, ...state];
      });
    }
  };
  useEffect(() => {

  }, [useFetchFromAPI, show]);


  return (
    <div className={classes.root}>
      <Comments
        setShowMore={setShowMore}
        nextPage={nextPage}
        prevPage={prevPage}
        lastPage={lastPage}
        setUrl={setUrl}
        page={page}>
        {loading ?
          (<CircularIndeterminate/>) :
          showMore ?
            data.map((c, index) => {
              console.log('Hello');
              return (
                <Comment
                  key={index}
                  name={c.name}
                  data={c.created_at}
                  text={c.text}/>
              );
            }) :
            state.map((c, index) => {
              console.log('State');

              return (
                <Comment
                  key={index}
                  name={c.name}
                  data={c.created_at}
                  text={c.text}/>
              );
            })
        }
        {
          page === lastPage ?
            null :
            <Button onClick={show}>
              Show More
            </Button>
        }

      </Comments>
    </div>
  );
};