import React, { useEffect, useState } from "react";
import { Button } from "@material-ui/core";
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import ArrowForwardIosIcon from '@material-ui/icons/ArrowForwardIos';
import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'row',
    margin: 10,
    border: "1px solid black"
  },
  p: {
    color: 'red'
  }
}));

export const Pagination = ({ page, nextPage, prevPage, setUrl, lastPage, setShowMore }) => {
  const classes = useStyles();
  const [count, setCount] = useState(page);

  const nextPageHandler = () => {
    setShowMore(false);
    setUrl(nextPage);
  };
  const prevPageHandler = () => {
    setShowMore(false);
    setUrl(prevPage);
  };
  const pageHandler = (index) => {
    setShowMore(false);
    setCount(index);
    console.log('index', index);
    setUrl(`https://jordan.ashton.fashion/api/goods/30/comments?page=${index}`);
  };

  useEffect(() => {
    setCount(page);
  }, [page]);

  const pages = Array
    .apply(null, Array(lastPage))
    .map(
      (item, index) => {
        return (
          <span
            key={index}
            style={{ cursor: 'pointer', margin: '5px' }}
            onClick={() => pageHandler(index + 1)}>
            <span className={count === index + 1 ? classes.p : ''}>{index + 1}</span>
          </span>
        );
      });
  return (
    <>
      <div>
        {count <= 1 ? '' : <Button onClick={prevPageHandler}>
          <ArrowBackIosIcon/>
        </Button>}
        {pages}
        {count >= lastPage ? '' :
          <Button onClick={nextPageHandler}>
            <ArrowForwardIosIcon/>
          </Button>}
      </div>
    </>

  );
}
;