
import { useState, useEffect } from "react";

export const useFetchFromAPI = () => {
  const [state, setState] = useState([]);
  const [page, setPage] = useState(null);
  const [lastPage, setLastPage] = useState(null);
  const [nextPage, setNextPage] = useState(null);
  const [prevPage, setPrevPage] = useState(null);
  const [loading, setLoading] = useState(false);
  const [url, setUrl] = useState(`https://jordan.ashton.fashion/api/goods/30/comments`);
  const [showMore, setShowMore] = useState(false);
  const [data, setData] = useState([]);

  useEffect(() => {
    const getComments = async () => {
      setLoading(true);
      const comments = await (await fetch(url)).json();
      setState(comments.data);
      setPage(comments.current_page);
      setLastPage(comments.last_page);
      setPrevPage(comments.prev_page_url);
      setNextPage(comments.next_page_url);
      setLoading(false);
      return comments;
    };
    getComments();
  }, [url, setData, showMore]);

  return [
    data,
    setData,
    showMore,
    setShowMore,
    page,
    prevPage,
    setState,
    nextPage,
    lastPage,
    state,
    loading,
    setUrl];
};